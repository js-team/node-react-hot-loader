# Installation
> `npm install --save @types/shallowequal`

# Summary
This package contains type definitions for shallowequal ( https://github.com/dashed/shallowequal ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/shallowequal

Additional Details
 * Last updated: Mon, 04 Feb 2019 22:02:18 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Sean Kelley <https://github.com/seansfkelley>, BendingBender <https://github.com/BendingBender>, Arnd Issler <https://github.com/arndissler>.
